﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_6_delegates
{

    public delegate void MessengerDelegate(string message);

    public class Account
    {
        private MessengerDelegate messenger;
        public string PersonFullName { get; set; }
        public int Sum { get; private set; } = 1;


        public void RegisterMessenger(MessengerDelegate messenger)
        {
            this.messenger += messenger;
        }

        public void UnregisterMessenger(MessengerDelegate messenger)
        {
            this.messenger -= messenger;
        }



        public void Add(int sum)
        {
            Sum += sum;
            if (messenger != null)
            {
                messenger.Invoke("Вы внесли " + sum);
            }
        }
        //или сокращенный вариант
        public void Withdraw(int sum)
        {
            if (sum > Sum)
            {
                messenger?.Invoke("Сумма больше " + sum);
                return;
            }

            Sum -= sum;
            messenger?.Invoke("Вы сняли " + sum);
        }

    }
}
