﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_6_delegates
{
    class Program
    {
        static void Main(string[] args)
        {
            Account account = new Account
            {
                PersonFullName = "Иванов Иван Иванович",
            };

            account.RegisterMessenger(new MessengerDelegate(Console.WriteLine));
            // альтернатива

            account.RegisterMessenger(new MessengerDelegate(new ConsoleMessenger().SendMessage));

            account.Add(500);
            account.Withdraw(200);

            Console.ReadLine();
        }
    }
}

